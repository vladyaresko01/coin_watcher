import type { NextPage } from 'next'

import CoinWatcher from '../components/CoinWatcher/CoinWatcher'
import Header  from '../components/Header/Header'
import Footer from '../components/Footer/Footer'
const Home: NextPage = () => {
  return (
    <div>
      <Header></Header>
      <CoinWatcher></CoinWatcher>
      <Footer></Footer>
    </div>
  )
}

export default Home
