import { createStore } from "redux";
import { createWrapper } from "next-redux-wrapper";

// const store = configureStore()
type Action = {
  type: string;
  payload: Coin;
};
type Coin = {
  url: string;
  name: string;
  price: string;
  currensy:string
};

const reducer = (state: Coin[] = [], action: Action) => { // created reduser
  switch (action.type) {
    case "ADD": { 
      if (state.find((item) => item.url == action.payload.url)) {//protection from cloning similar data. if the item already exists we don't add a copy
        return state;
      } else {
        return state.concat(action.payload); //used concat to prevent mutating
      }
    }

    case "UPDATE": {
      let item = state.find((item) => item.url == action.payload.url);
      if (item) {
        let tempState = [...state];
        tempState[state.indexOf(item)] = action.payload;
        return tempState;
      } else {
        return state;
      }
    }
    default:
      return state;
  }
};
export const store = createStore(reducer);
store.subscribe(() => {
  // console.log("current state", store.getState());
});

const makeStore = () => store;
export const wrapper = createWrapper(makeStore);
