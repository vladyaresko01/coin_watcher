# Coin Watcher
## Mini app to show crypto price in real time


## Tech
- [React](https://reactjs.org/)
- [typescript]
- [Next.js] 
- [docker]-
## Installation
Install the dependencies and start the server @@ docker.

start build

```sh
npm i
npm run build
npm start
```

start dev

```sh
npm i
npm run dev
```

install and deploy in a Docker container.

```sh
 docker build -t nextjs-docker .   
 docker run -p 3000:3000 nextjs-docker
```


## Coin Watcher

