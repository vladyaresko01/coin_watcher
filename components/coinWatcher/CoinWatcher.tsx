import React from "react";
import s from "./CoinWatcher.module.scss";
import CoinInstance from "../CoinInstance/CoinInstance";

type Props = {};
// url`s to get token information and theit update rate
let list = [ 
  { url: "https://api.coinbase.com/v2/prices/BTC-USD/buy", uptadeRate: 6000 },
  { url: "https://api.coinbase.com/v2/prices/ETC-USD/buy", uptadeRate: 5000 },
  { url: "https://api.coinbase.com/v2/prices/ETH-USD/buy", uptadeRate: 4000 },
  { url: "https://api.coinbase.com/v2/prices/USDT-USD/buy", uptadeRate: 10000 },
  { url: "https://api.coinbase.com/v2/prices/ADA-USD/buy", uptadeRate: 5000 },
  { url: "https://api.coinbase.com/v2/prices/SOL-USD/buy", uptadeRate: 7000 },
  { url: "https://api.coinbase.com/v2/prices/DOGE-USD/buy", uptadeRate: 5000 },
  { url: "https://api.coinbase.com/v2/prices/WBTC-USD/buy", uptadeRate: 5000 },
  { url: "https://api.coinbase.com/v2/prices/LTC-USD/buy", uptadeRate: 5000 },
  // {url :"https://api.coindesk.com/v1/bpi/currentprice/CNY.json",uptadeRate:5000},
];

export default function CoinWatcher({}: Props) {
  // map function to show as many coin elements as we have in array
  let items: JSX.Element[] = list.map((item, i) => {
    return (
      <CoinInstance
        key={i}
        pUrl={item.url}
        updR={item.uptadeRate}
      ></CoinInstance>
    );
  });

  return <div className={s.container}>{items}</div>;
}
