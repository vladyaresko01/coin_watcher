import React from 'react'
import s from "./Footer.module.scss"
type Props = {}

export default function Footer({}: Props) {
  return (
    <div className={s.footer}>Track Token price in real time</div>
  )
}