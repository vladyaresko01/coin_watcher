import React, {useEffect } from "react";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import s from "./CoinInstance.module.scss";
//describing types
type Props = {
  pUrl: string;
  updR: number;
};
type Coin = {
  url: string;
  name: string;
  price: number;
  curensy: string;
};
/// function that import dispatch function, url, action type then make axios get than pass action + payload to redux
function setState(func: Function, type: string, url: string) {
  axios.get(url).then((ans) => {
    let obj = {
      url: url,
      name: ans.data.data.base,
      price: ans.data.data.amount,
      curensy: ans.data.data.currency,
    };
    func({
      type: type,
      payload: obj,
    });
  });
}

export default function CoinInstance({ pUrl, updR }: Props) {
  const dispatch = useDispatch();
  const coin = useSelector((state: Coin[]) =>
    state.find((obj) => obj.url == pUrl)
  );

  useEffect(() => {
    setState(dispatch, "ADD", pUrl);
    setInterval(() => {
      setState(dispatch, "UPDATE", pUrl);
    }, updR);
  }, []);

  if (coin != undefined) {
    return ( // show  a skeleton if info is loading for pretier view
      <div className={s.element}>
        <div className={s.text}>{coin.name}</div>
        <div className={s.text}>
          price {coin.price} {coin.curensy}
        </div>
      </div>
    );
  } else {
    return (
      <div className={s.element}>
        <div className={s.text}>loading</div>
        <div className={s.text}>price loading</div>
      </div>
    );
  }
}
