import React from 'react'
import s from './Header.module.scss'
type Props = {}

export default function Header({}: Props) {
  return (
    <h1 className={s.header}>Coin price checker</h1>
  )
}