FROM node:16.13-alpine3.12 as builder
WORKDIR /app
COPY package.json ./
RUN npm i
RUN npm i sass
RUN npm i redux
RUN npm i --save-dev @types/react
COPY . .
COPY styles ./
EXPOSE 3000
RUN npm run build
CMD ["npm", "start"]
